public class MyDateTime extends Object {
    MyDate date;
    MyTime time;

    public MyDateTime(MyDate date, MyTime myTime) {
        super();
        this.date = date;
        this.time = myTime;
    }

    public String toString() {
        return date + " " + time;
    }

    public void incrementDay() {
        date.incrementDay();
    }

    public void incrementHour() {
        incrementHour(1);
    }

    public void incrementHour(int diff) {
        int dayDiff = time.incrementHour(diff);
        if (dayDiff < 0) {
            date.decrementDay(-dayDiff);
        } else {
            date.incrementDay(dayDiff);
        }
    }

    public void decrementHour(int diff) {
        incrementHour(-diff);
    }

    public void incrementMinute(int diff) {
        int dayDiff = time.incrementMinute(diff);
        if (dayDiff < 0) {
            date.decrementDay(-dayDiff);
        } else {
            date.incrementDay(dayDiff);
        }
    }

    public void decrementMinute(int diff) {
        incrementMinute(-diff);
    }

    public void incrementYear(int diff) {
        date.incrementYear(diff);
    }

    public void decrementDay() {
        date.decrementDay();
    }

    public void decrementYear() {
        date.decrementYear();
    }

    public void decrementMonth() {
        date.decrementMonth();
    }

    public void incrementDay(int i) {
        date.incrementDay(i);
    }

    public void decrementMonth(int i) {
        date.decrementMonth(i);
    }

    public void decrementDay(int i) {
        date.decrementDay(i);
    }

    public void incrementMonth(int i) {
        date.incrementMonth(i);
    }

    public void decrementYear(int i) {
        date.decrementYear(i);
    }

    public void incrementMonth() {
        date.incrementMonth();
    }

    public void incrementYear() {
        date.incrementYear();
    }

    public boolean isBefore(MyDateTime anotherDateTime) {
        return time.getHour() < anotherDateTime.time.getHour();
    }

    public boolean isAfter(MyDateTime anotherDateTime) {
        boolean isAfterForHour = true;
        if (date.year == anotherDateTime.date.year && date.month == anotherDateTime.date.month && date.day == anotherDateTime.date.day) {
            if (time.getHour() < anotherDateTime.time.getHour()) {
                isAfterForHour = false;
            } else if (time.getHour() == anotherDateTime.time.getHour() && time.getMinute() < anotherDateTime.time.getMinute()) {
                isAfterForHour = false;
            }
        }
        return time.getHour() > anotherDateTime.time.getHour();
    }

    public String dayTimeDifference(MyDateTime anotherDateTime) {
        String hourDiff  = null;
        String minuteDiff = null;

        if (date.year == anotherDateTime.date.year && date.month == anotherDateTime.date.month && date.day == anotherDateTime.date.day) {
            hourDiff = String.valueOf(anotherDateTime.time.getHour() - time.getHour());
            minuteDiff = String.valueOf(anotherDateTime.time.getMinute() - time.getMinute());
        }

        return  hourDiff +" hour(s) " + minuteDiff + " minute(s)";
    }
}


