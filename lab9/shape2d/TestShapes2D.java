package shape2d;

public class TestShapes2D {
    public static void main(String[] args) {
        Circle c1 = new Circle(5);
        c1 = new Circle(6); // not updating upper one.
        Circle c2 = new Circle(6);
        String str = new String("Hello"); //inmutable
        String str2 = "Hola";
        // str == str2 compares object not content
        System.out.println(str.equals(str2)); // Correct Comparison


        System.out.println("c1 == c2" + (c1 == c2));
        System.out.println("c1.equals(c2)" + c1.equals(c2));


    }
}
