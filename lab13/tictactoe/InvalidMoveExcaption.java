package tictactoe;

public class InvalidMoveExcaption extends Exception{
    public InvalidMoveExcaption(String message){
        super(message);
    }
}
