package tictactoe;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args)  {
        Scanner reader = new Scanner(System.in);

        Board board = new Board();

        System.out.println(board);
        while (!board.isEnded()) {

            int player = board.getCurrentPlayer();
            boolean invalidRow = false;
            int row= 0;
            int col= 0;

            do {
                System.out.print("Player " + player + " enter row number:");
                try {
                    row = Integer.valueOf(reader.nextLine());
                    System.out.println("Valid integer");
                    invalidRow = false;
                } catch (NumberFormatException ex) {
                    System.out.println("invalid integer");
                    invalidRow = true;
                }
            }while (invalidRow);

            do {
                System.out.print("Player " + player + " enter column number:");
                try {
                     col = Integer.valueOf(reader.nextLine());
                    System.out.println("Valid integer");
                    invalidRow = false;
                }catch (NumberFormatException ex){
                System.out.println("invalid integer");
                invalidRow = true;}
            }while (invalidRow);


            try {
                board.move(row, col);
            } catch (InvalidMoveExcaption e) {
                System.out.println(e.getMessage());
            }
            System.out.println(board);
        }


        reader.close();
    }


}
