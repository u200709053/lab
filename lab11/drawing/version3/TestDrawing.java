package drawing.version3;

import drawing.version2.Drawing;
import shapes.Circle;
import shapes.Rectangle;
import shapes.Shape;
import shapes.Square;

public class TestDrawing {

	public static void main(String[] args) {
			drawing.version2.Drawing drawing = new Drawing();

			drawing.addShape(new Circle(5));
			drawing.addShape(new Square(5));
			drawing.addShape(new Rectangle(5,6));
			//drawing.addShape(new Integer(2);
			//drawing.addShape("Hello");
			//drawing.addShape(new Shape);

			System.out.println("Total area = " + drawing.calculateTotalArea());
		}
	}

